--  El ingreso total recibido por cada actor, sin
--  importar en cuantas películas haya participado.
SELECT nombre, SUM(sueldo) AS 'Sueldo Total'
FROM Elenco
GROUP BY nombre

-- El monto total destinado a películas por cada
-- Estudio Cinematográfico, durante la década de los 80's.
SELECT año, SUM(presupuesto) AS 'Presupuesto Total'
FROM Película
WHERE año >= 1/1/1980 AND año < 1/1/1990

-- Nombre y sueldo promedio de los actores (sólo hombres)
-- que reciben en promedio un pago superior a 5 mdd
-- por película.
SELECT nombre, AVG(sueldo) AS 'Sueldo Promedio'
FROM Elenco E, Actor A
WHERE E.nombre = A.nombre AND A.sexo = 'M'
GROUP BY nombre
HAVING AVG(sueldo) > 5000000
ORDER BY [Sueldo Promedio] DESC

-- Título y año de producción de las películas con
-- menor presupuesto. Es decir, la película de
-- Titanic se ha producido en varios años, debe
-- de mostrar la producción y año con menor
-- presupuesto, y así para cada película.
SELECT título, MIN(presupuesto) AS 'Presupuesto Mínimo'
FROM Película
GROUP BY título

-- Mostrar el sueldo de la actriz mejor pagada.
SELECT nombre, sueldo
FROM Elenco
WHERE sueldo = (SELECT MAX(sueldo) FROM Elenco)