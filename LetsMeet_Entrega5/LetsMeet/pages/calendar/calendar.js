var nameid='';
var calendar;
var buscaNombre = "Alondra";


angular.module('rtfmApp')
.controller('calendarPageController', function($scope, $location, $routeParams, userService, fb, $firebaseAuth, $firebaseObject){
    var ref = new Firebase(fb.url);
    var authObj = $firebaseAuth(ref);
	$scope.user = userService.getLoggedInUser();
    nameid = $scope.user.id;
    var refCal = new Firebase('https://famc-lets-meet.firebaseio.com/user/' + nameid + '/calendar');
    //
    var refUsuario = new Firebase('https://famc-lets-meet.firebaseio.com/user');
    $scope.Usuario = $firebaseObject(refUsuario);
    console.log($scope.Usuario);
	refCal.once("value", function(snapshot){
		snapshot.forEach(function(childSnapshot){
			var key = childSnapshot.key();
			var childData = childSnapshot.val();
			calendar = childData;
			var obj = JSON.parse(calendar);
            obj.push(childData);
            calendar = JSON.stringify(obj);
			console.log(calendar);
		});
	});
	
	/*
	//Función para buscar usuarios
	refUsuario.once("value", function(snapshot){
		snapshot.forEach(function(childSnapshot){
			var childData = childSnapshot.val();
			if(childData.givenName == buscaNombre){
				console.log("Eureka putos!");
			}
		});
	});*/
	
}); 

var ref = new Firebase("https://famc-lets-meet.firebaseio.com");

$(document).ready(function() {
    
    
		$('#calendar').fullCalendar({
		    
		    googleCalendarApiKey: 'AIzaSyAB2apK2-QGSCr_NBkW-_Qt4cdqLuaWsE8',
        eventSources: [
            {
                googleCalendarId: 'aiii01jg90c4oiq1jrk9idvai0@group.calendar.google.com',
                color: '#dd4b39'
            },
            {
                googleCalendarId: 'sharite96@gmail.com',

            }
        ],
		    
           
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
		 /* 	eventMouseover: function(event, jsEvent, view) {
                if (view.name !== 'agendaDay') {
                    $(jsEvent.target).attr('title', event.title);
                }
            },   
  			eventMouseover : function(data, event, view) {
            var content = '<h3>'+data.title+'</h3>' + 
                '<p><b>Start:</b> '+data.start+'<br />' + 
                (data.end && '<p><b>End:</b> '+data.end+'</p>' || '');

            tooltip.set({
                'content.text': content
            })
            .reposition(event).show(event);
        },*/
            eventClick: function(calEvent, jsEvent, view){
                var r = confirm("Eliminar " + calEvent.title);
                if (r === true){
                      $('#calendar').fullCalendar('removeEvents', calEvent._id).remove();
                  }
            },

			selectable: true,
            selectHelper: true,
			select: function(start, end, events) {
                
				var title = prompt('Título del evento:');
                var aux;
                var eventData;
				if (title) {
				    aux = {
				        title: title,
				        start: start,
				        end: end
				    };
				    for(var i = 0; i < 7; i++){
				        if(start._i[i] < 10){
				            start._i[i] = "0" + start._i[i];
				        }
				        if(end._i[i] < 10){
				            end._i[i] = "0" + end._i[i];
				        }
				    }
					eventData = {
						title: title,
						start: start._i[0] + '-' + start._i[1] + '-' + start._i[2] + '-' + start._i[3] + '-' + start._i[4] + '-' + start._i[5] + '-' + start._i[6],
						end: end._i[0] + '-' + end._i[1] + '-' + end._i[2] + '-' + end._i[3] + '-' + end._i[4] + '-' + end._i[5] + '-' + end._i[6]
					};
					
                    $('#calendar').fullCalendar('renderEvent', aux, true); // stick? = true
				}
				$('#calendar').fullCalendar('unselect');
                ref.child('user/' + nameid + '/calendar').push(eventData);
			},
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: calendar
			/*
	    	events: [
                    {
                        title: 'EVENTO DE FERMIN',
                        start: '2016-03-16',
                        color: 'pink'
                        
                    },
                    {
                        title: 'EVENTO 2',
                        start: '2016-03-20',
                        color: 'green'
                        
                    },
                    {
                        title: 'EVENTO 3',
                        start: '2016-03-03',
                        color: 'red'
                        
                    }
		    ] */
			
		});
		
	});