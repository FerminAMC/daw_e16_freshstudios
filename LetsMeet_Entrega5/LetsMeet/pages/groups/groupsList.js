angular.module('rtfmApp')
.controller('groupsListPageController', function($scope, userService, $firebaseObject){
    var refUsuario = new Firebase('https://famc-lets-meet.firebaseio.com/user');
    
    $scope.user = userService.getLoggedInUser();
	
    $scope.logout = function(){        
        userService.logout();
    }
    
    $scope.grupos;

    var groups = [
        {Name: "DAW/BD"},
        {Name: "Proyecto Videojuegos"},
        {Name: "Equipo Redes"},
        {Name: "FIS"}
        
    ];
    
    $scope.groups = groups;
});