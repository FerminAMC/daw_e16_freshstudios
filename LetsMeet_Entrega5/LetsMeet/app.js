angular.module('rtfmApp', ['ngRoute', 'firebase', 'ui.calendar', 'tb-color-picker'])
.constant('fb', {
  url: 'https://famc-lets-meet.firebaseio.com/'
})
.config(function($routeProvider){
        $routeProvider.
            when('/login', {
                templateUrl: 'pages/login/login.html'
            }).
            when('/main', {
                templateUrl: 'pages/main/main.html'
            }).
            when('/calendar', {
                templateUrl: 'pages/calendar/calendar.html'
            }).
            when('/groupsList', {
                templateUrl: 'pages/groups/groupsList.html'
            }).
            when('/group', {
                templateUrl: 'pages/groups/group.html'
            }).
            when('/toDo', {
               templateUrl: 'pages/toDo/tareas_perfil.html'
            }).
            
            
            otherwise({
                redirectTo: '/login'
            });
            
            
});

    