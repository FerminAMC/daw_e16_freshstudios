angular.module('rtfmApp').service('userService', function($firebaseAuth, $location, fb){

	var ref = new Firebase(fb.url);
	var refUser = new Firebase(fb.url+ "/user");
    var authObj = $firebaseAuth(ref);
	
	var user = {
		accessToken: '',
		id: '',
		fullName: '',
		familyName: '',
		givenName: '',
		photo: '',
		todos: '',
		calendar: ''
	};
	
	
	var info = authObj.$getAuth();
	if(info){
		user.accessToken = info.google.accessToken;
		user.fullName = info.google.displayName;
		user.familyName = info.google.cachedUserProfile.family_name;
		user.givenName = info.google.cachedUserProfile.given_name;
		user.photo = info.google.profileImageURL;
		user.id = info.google.id;
	}
	
    this.getLoggedInUser = function(){
        return user;
    }
	
	this.loginWithGoogle = function(){
		authObj.$authWithOAuthPopup("google").then(function(authData) {
		  user.accessToken = authData.google.accessToken;
		  user.fullName = authData.google.displayName;
		  user.familyName = authData.google.cachedUserProfile.family_name;
		  user.givenName = authData.google.cachedUserProfile.given_name;
		  user.id = authData.google.id;
		  user.photo = authData.google.profileImageURL;
		  ref.child("user/" + authData.google.id).once('value', function(snapshot){
		  	var exists = (snapshot.val() !== null);
		  	if(!exists){
		  		ref.child("user/" + authData.google.id).set(user);
		  	}
		  });
          $location.path('main');
		}).catch(function(error) {
		  console.error("Authentication failed:", error);
		});
	}
	
	this.logout = function(){
		authObj.$unauth();
        $location.path('login');
	}
	
});