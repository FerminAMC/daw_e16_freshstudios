var nameid='';
var calendar = '';


angular.module('rtfmApp')
.controller('calendarPageController', function($scope, $location, $routeParams, userService, fb, $firebaseAuth, $firebaseArray){
    var ref = new Firebase(fb.url);
    var authObj = $firebaseAuth(ref);
	$scope.user = userService.getLoggedInUser();
    nameid = $scope.user.id;
    console.log($scope.user.id);
    var refUser = new Firebase('https://famc-lets-meet.firebaseio.com/user' + nameid + '/calendar');
    $scope.calendar = $firebaseArray(refUser);
    calendar = $scope.calendar.expires;
    console.log($scope.calendar);
	
}); 

var ref = new Firebase("https://famc-lets-meet.firebaseio.com");
$(document).ready(function() {
    
		$('#calendar').fullCalendar({
		    
		    googleCalendarApiKey: 'AIzaSyAB2apK2-QGSCr_NBkW-_Qt4cdqLuaWsE8',
        eventSources: [
            {
                googleCalendarId: 'aiii01jg90c4oiq1jrk9idvai0@group.calendar.google.com'
            },
            {
                googleCalendarId: 'sharite96@gmail.com'

            }
        ],
		    
           
			header: {
				left: 'prev,next today',
				right: 'month,agendaWeek,agendaDay'
			},
		 /* 	eventMouseover: function(event, jsEvent, view) {
                if (view.name !== 'agendaDay') {
                    $(jsEvent.target).attr('title', event.title);
                }
            },*/    
  			eventMouseover : function(data, event, view) {
            var content = '<h3>'+data.title+'</h3>' + 
                '<p><b>Start:</b> '+data.start+'<br />' + 
                (data.end && '<p><b>End:</b> '+data.end+'</p>' || '');

            tooltip.set({
                'content.text': content
            })
            .reposition(event).show(event);
        },
            eventClick: function(calEvent, jsEvent, view){
                var r = confirm("Delete " + calEvent.title);
                if (r === true){
                      $('#calendar').fullCalendar('removeEvents', calEvent._id);
                  }
            },

			selectable: true,
			selectHelper: true,
			select: function(start, end) {
                
				var title = prompt('Título del evento:');
                
                var eventData;
				if (title) {
					eventData = {
						title: title,
						start: start,
						end: end
					};
					
                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
				}
				$('#calendar').fullCalendar('unselect');
                ref.child('user/' + nameid + '/calendar').push(eventData);
			},
			editable: true,
			eventLimit: true, // allow "more" link when too many events
	    	/* events: [
                    {
                        title: 'EVENTO DE FERMIN',
                        start: '2016-03-16',
                        color: 'pink'
                        
                    },
                    {
                        title: 'EVENTO 2',
                        start: '2016-03-20',
                        color: 'green'
                        
                    },
                    {
                        title: 'EVENTO 3',
                        start: '2016-03-03',
                        color: 'red'
                        
                    }
		    ] */
			
		});
		
	});
