angular.module('rtfmApp')
.controller('mainPageController', function($scope, userService, $location){
	
    $scope.user = userService.getLoggedInUser();
	
    $scope.logout = function(){        
        userService.logout();
    }

});