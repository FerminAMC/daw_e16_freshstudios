angular.module('rtfmApp', ['ngRoute', 'firebase'])
.constant('fb', {
  url: 'https://famc-lets-meet.firebaseio.com/'
})
.config(function($routeProvider){
        $routeProvider.
            when('/login', {
                templateUrl: 'pages/login/login.html'
            }).
            when('/main', {
                templateUrl: 'pages/main/main.html'
            }).
            when('/calendar', {
                templateUrl: 'pages/calendar/calendar.html'
            }).
            when('/groupsList', {
                templateUrl: 'pages/groups/groupsList.html'
            }).
            when('/calendar', {
                templateUrl: 'pages/groups/group.html'
            }).
            
            otherwise({
                redirectTo: '/login'
            });
});
