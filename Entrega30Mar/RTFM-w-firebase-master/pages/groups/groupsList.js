angular.module('rtfmApp')
.controller('groupsListPageController', function($scope, userService){
    $scope.user = userService.getLoggedInUser();
	
    $scope.logout = function(){        
        userService.logout();
    }

});