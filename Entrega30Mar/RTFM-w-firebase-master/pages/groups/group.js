angular.module('rtfmApp')
.controller('groupPageController', function($scope, userService){
    $scope.user = userService.getLoggedInUser();
	
    $scope.logout = function(){        
        userService.logout();
    }

});