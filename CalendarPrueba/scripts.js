//día del mes
$(document).ready(function(){
	var w = new Date();
	var month = w.getMonth();
	var year = w.getFullYear();
	var week = new Date(year, month, 1).getDay();
	for(var i = week; i < 42; i++){
		var d = new Date(year,month,i);
		var day = d.getDate();
		$("#day" + i).text(function(){
			return day;
		});
	}
});

//nombre del mes
$(document).ready(function(){
	var d = new Date();
	var month = d.getMonth();
	var year = d.getFullYear();
	$("#month").text(function(){
		switch(month){
			case 0 : return "enero";
				break;
			case 1 : return "febrero";
				break;
			case 2 : return "marzo";
				break;
			case 3 : return "abril";
				break;
			case 4 : return "mayo";
				break;
			case 5 : return "junio";
				break;
			case 6 : return "julio";
				break;
			case 7 : return "agosto";
				break;
			case 8 : return "septiembre";
				break;
			case 9 : return "octubre";
				break;
			case 10 : return "noviembre";
				break;
			case 11 : return "diciembre";
				break;
		}
	});
	$("#year").text(function(){
		return year;
	})
});
	


